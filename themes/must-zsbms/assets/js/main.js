'use strict';

$('document').ready(function () {
  var $win = $(window);
  var $main = $('main');
  var $body = $('body');
  var $backdrop = $('.backdrop__bar');
  var $btnMenu = $('.btn-menu');
  var $menuItem = $('.aside-menu');
  var $contentHeader = $('.content-header');
  var $btnFuncionality = $('.btn-funcionality');

  var isScrolled = function isScrolled() {
    return $win.scrollTop() > 200;
  };
  var isDesktop = function isDesktop() {
    return $win.outerWidth() >= 768;
  };
  function scrollToContent(element, value) {
    $('html, body').animate({
      scrollTop: $('.' + element).offset().top - value
    }, 800);
  }

  /* SLIDERS DEFAULT */
  var swiperImages = new Swiper('.swiper-images', {
    direction: 'horizontal',
    loop: false,
    watchOverflow: true,
    observer: true,
    observeParents: true,
    speed: 600,
    slidesPerView: 3,
    autoplay: {
      delay: 5000
    },
    clickable: true,
    breakpoints: {
      991: {
        slidesPerView: 2.2,
        slidesPerGroup: 1,
        loop: true
      }
    },
    navigation: {
      nextEl: '.swiper-button-next-images',
      prevEl: '.swiper-button-prev-images'
    }
  });
  var swiperFaqs = new Swiper('.swiper-faqs', {
    direction: 'horizontal',
    loop: false,
    watchOverflow: true,
    observer: true,
    observeParents: true,
    speed: 600,
    slidesPerView: 1,
    autoplay: {
      delay: 5000
    },
    clickable: true,
    navigation: {
      nextEl: '.swiper-button-next-faqs',
      prevEl: '.swiper-button-prev-faqs'
    }
  });
  /* *************** */

  /* FUNCTION DEFAULT */
  var itemOpen = function itemOpen() {
    $backdrop.removeClass('d-none');
    $body.addClass('modal-open');
  };
  var itemClose = function itemClose() {
    $backdrop.addClass('d-none');
    $body.removeClass('modal-open');
  };
  var menuOpen = function menuOpen() {
    $menuItem.addClass('aside-menu-active');
    itemOpen();
  };
  var menuClose = function menuClose() {
    $menuItem.removeClass('aside-menu-active');
    itemClose();
  };
  var headerScrolledOpen = function headerScrolledOpen() {
    $contentHeader.addClass('content-header-scrolled');
  };
  var headerScrolledClose = function headerScrolledClose() {
    $contentHeader.removeClass('content-header-scrolled');
  };
  /* ****************** */

  $btnMenu.click(function () {
    if ($menuItem.hasClass('aside-menu-active')) {
      $btnMenu.find('span').removeClass('active');
      $btnMenu.find('p').text('Menu');
      menuClose();
    } else {
      $btnMenu.find('span').addClass('active');
      $btnMenu.find('p').text('Fechar');
      menuOpen();
    }
  });

  $win.scroll(function () {
    if (isScrolled()) {
      headerScrolledOpen();
    } else {
      headerScrolledClose();
    }
  });

  /* BUTTON SCROLABLE */
  $btnFuncionality.click(function () {
    if (isDesktop()) {
      scrollToContent('content-home-functionalities', 90);
    } else {
      scrollToContent('content-home-functionalities', 0);
    }
    $btnMenu.find('span').removeClass('active');
    $btnMenu.find('p').text('Menu');
    menuClose();
  });
  if (window.location.hash == "#linkFuncionalidade") {
    if (isDesktop()) {
      scrollToContent('content-home-functionalities', 90);
    } else {
      scrollToContent('content-home-functionalities', 0);
    }
    $btnMenu.find('span').removeClass('active');
    $btnMenu.find('p').text('Menu');
    menuClose();
  }
  /* *********************** */

  AOS.init({
    offset: 0,
    delay: 0,
    duration: 950,
    easing: 'ease-in-sine'
  });
});