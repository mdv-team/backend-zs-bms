<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMustPagesHighlights extends Migration
{
    public function up()
    {
        Schema::table('must_pages_highlights', function($table)
        {
            $table->string('url', 200);
            $table->boolean('active')->nullable()->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('must_pages_highlights', function($table)
        {
            $table->dropColumn('url');
            $table->dropColumn('active');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
            $table->dropColumn('title');
            $table->dropColumn('description');
        });
    }
}
