<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMustPagesContents extends Migration
{
    public function up()
    {
        Schema::table('must_pages_contents', function($table)
        {
            $table->string('url_full', 300)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('must_pages_contents', function($table)
        {
            $table->dropColumn('url_full');
        });
    }
}
