<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMustPagesBanners extends Migration
{
    public function up()
    {
        Schema::table('must_pages_banners', function($table)
        {
            $table->string('title', 255)->nullable();
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('must_pages_banners', function($table)
        {
            $table->dropColumn('title');
            $table->dropColumn('description');
        });
    }
}
