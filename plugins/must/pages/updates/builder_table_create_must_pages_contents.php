<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMustPagesContents extends Migration
{
    public function up()
    {
        Schema::create('must_pages_contents', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('url', 200)->nullable();
            $table->text('text')->nullable();
            $table->string('hash', 200)->nullable();
            $table->string('hash_relative', 200)->nullable();
            $table->string('type', 150)->nullable();
            $table->string('location', 200)->nullable();
            $table->string('identifier', 150)->nullable();
            $table->string('section', 150)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('must_pages_contents');
    }
}
