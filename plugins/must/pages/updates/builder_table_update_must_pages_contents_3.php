<?php namespace Must\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMustPagesContents3 extends Migration
{
    public function up()
    {
        Schema::table('must_pages_contents', function($table)
        {
            $table->dropColumn('image');
        });
    }
    
    public function down()
    {
        Schema::table('must_pages_contents', function($table)
        {
            $table->string('image', 191)->nullable();
        });
    }
}
