<?php namespace Must\Pages\Models;

use Model;

/**
 * Model
 */
class Banner extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'must_pages_banners';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'url' => 'required',
    ];

    public $attachOne = [
        'image_desktop' => [
            'System\Models\File',
            'delete' => true,
        ],
        'image_mobile' => [
            'System\Models\File',
            'delete' => true,
        ],
    ];

    public function getUrlOptions()
    {
        $data = [];

        $content = Content::distinct('url')
            ->get();

        if ($content) {
            foreach ($content as $key => $value) {
                $data[$value->url] = $value->url;
            }
        }

        return $data;
    }

    public function beforeSave()
    {
        if (!empty($this->url)) {
            $this->url = str_replace(["https://", "http://", "/", "www."], "", $this->url);
        }
    }
}
