<?php namespace Must\Pages\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Theme;
use Must\Pages\Models\Banner;

class Banners extends ComponentBase
{
    public $banners;

    public function componentDetails()
    {
        return [
            'name' => 'Banners Component',
            'description' => 'No description provided yet...',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->banners = $this->getBannersActive();
    }

    public function getBannersActive()
    {
        $theme = Theme::getActiveTheme();
        $url = $theme->getConfigValue('url');    
        $url = str_replace(["https://", "http://", "/", "www."], "", $url);

        $banners = Banner::where('url', $url)
            ->where('active', 1)
            ->get();

        return $banners;
    }
}
