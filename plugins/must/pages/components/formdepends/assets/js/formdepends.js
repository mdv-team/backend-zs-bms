var FormDepends = {
	construct: function(){
		this.eventsDependsOn();
	},

	eventsDependsOn: function(){
		var self = this;

		$(".data-load-dynamic").on("change", function(){
			var $select = $(this);
			var toField = $select.attr('data-load-field');
			var toModel = $select.attr('data-load-model');
			var selected = $select.attr('data-load-selected');	
			var defaultValue = $select.attr('data-load-default');	

			if(typeof defaultValue != 'undefined' && defaultValue.indexOf(',') != -1){
				defaultValue = defaultValue.split(',');				
			}	

			if(toField.indexOf(',') != -1){
				var fields = toField.split(',');
				var models = toModel.split(',');
				var selects = typeof selected == 'undefined' ? [] : selected.split(',');

				$.each(fields, function(key, value){
					var selectValue = Array.isArray(defaultValue) ? defaultValue[key] : defaultValue;

					self._handleDataSelect(value, models[key], selects[key], selectValue, $select);	
				})
			}else{
				self._handleDataSelect(toField, toModel, selected, defaultValue, $select);
			}
		}).trigger('change');
	},

	_handleDataSelect: function(toField, toModel, toSelected, defaultValue, $select){	
		var	self = this;

		var value = $select.val() == '' ?  $select.attr('data-load-value') : $select.val();
		var loading = typeof $select.attr('data-load-loading') != 'undefined' ? $select.attr('data-load-loading') : 'Loading...';

		$(toField).html('<option value="">'+loading+'</option>');

		self._getDataSelect(toModel, value, 
			function(json){

			var selected = toSelected;
			    	
	    	defaultValue = typeof defaultValue != 'undefined' ? defaultValue : '---';
	    	
	    	var options = '<option value="">'+defaultValue+'</option>';

	        if(Object.keys(json).length > 0){
	        	
	        	$.each(json, function(key, value){
	        		if(selected == key){
	        			options += '<option value="'+key+'" selected>'+value+'</option>';
	        		}else{
	        			options += '<option value="'+key+'">'+value+'</option>';
	        		}			        					        	
	        	});

	        	$(toField).html(options);
	        }else{
	        	$(toField).html(options);
	        }

		});
	},

	_getDataSelect: function(field, value, cb){
		$.request('onLoadFilterDropdown', {
			data: {
				field: field,
				value: value,
				customer_id: $('#filter_customer_id').val(),
			},
		    success: function(json) {
		    	cb(json);
		    }
		})
	}
}

$(function(){
	FormDepends.construct();
})
