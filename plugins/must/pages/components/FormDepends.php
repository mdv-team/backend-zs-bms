<?php namespace Must\Pages\Components;

use Cms\Classes\ComponentBase;
use Must\Candidates\Models\EducationNivel;
use Must\Candidates\Models\Gender;
use Must\Candidates\Models\NivelExperience;
use Must\Candidates\Models\SocialMidia;
use Must\Candidates\Models\Time;
use Must\Registers\Models\Attribute;
use Must\Registers\Models\Category;
use Must\Registers\Models\CategoryFunction;
use Must\Registers\Models\Contract;
use Must\Registers\Models\Country;
use Must\Registers\Models\District;
use Must\Registers\Models\Experience;
use Must\Registers\Models\Hour;
use Must\Registers\Models\Knowledge;
use Must\Registers\Models\Language;
use Must\Registers\Models\Month;
use Must\Registers\Models\Salary;
use Must\Registers\Models\Zona;

class FormDepends extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'FormDepends Component',
            'description' => 'No description provided yet...',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs('/plugins/must/pages/components/formdepends/assets/js/formdepends.js');
    }

    public function onLoadFilterDropdown()
    {
        $payload = request()->all();

        $data = [];

        try {
            if (!isset($payload['value']) || empty($payload['value'])) {
                return $data;
            }

            switch ($payload['field']) {
                case 'Zones':
                    $data = Zona::where('district_id', $payload['value'])->lists('name', 'id');
                    break;

                case 'CategoryFunctions':
                    $data = CategoryFunction::where('category_id', $payload['value'])->lists('name', 'id');
                    break;

                case 'Knowledges':
                    $data = Knowledge::where('category_id', $payload['value'])->lists('name', 'id');
                    break;
            }
        } catch (Exception $e) {
        }

        return response()->json($data);
    }

    public function getDataToOptions($model)
    {
        switch ($model) {
            case 'Language':
                return Language::orderBy('name')->lists('name', 'id');
                break;

            case 'Country':
                return Country::orderBy('name')->lists('name', 'id');
                break;

            case 'Month':
                return Month::orderBy('id')->lists('name', 'id');
                break;

            case 'Time':
                return Time::orderBy('id')->lists('name', 'id');
                break;

            case 'Hour':
                return Hour::orderBy('name')->lists('name', 'id');
                break;

            case 'Contract':
                return Contract::orderBy('name')->lists('name', 'id');
                break;

            case 'District':
                return District::orderBy('name')->lists('name', 'id');
                break;

            case 'Category':
                return Category::orderBy('name')->lists('name', 'id');
                break;

            case 'Experience':
                return Experience::orderBy('name')->lists('name', 'id');
                break;

            case 'Attribute':
                return Attribute::orderBy('name')->lists('name', 'id');
                break;

            case 'EducationNivel':
                return EducationNivel::orderBy('id')->lists('name', 'id');
                break;

            case 'NivelExperience':
                return NivelExperience::orderBy('name')->lists('name', 'id');
                break;

            case 'Gender':
                return Gender::orderBy('name')->lists('name', 'id');
                break;

            case 'SocialMidia':
                return SocialMidia::orderBy('name')->lists('name', 'id');
                break;

            case 'Salary':
                $models = Salary::orderBy('sort_order')->get();
                $data = [];

                foreach ($models as $key => $value) {
                    $data[$value->id] = $value->name;
                }

                return $data;
                break;
        }
    }
}
