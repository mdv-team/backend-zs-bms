<?php namespace Must\Leads;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Must\Leads\Components\Leads' => 'leads'
        ];
    }

    public function registerSettings()
    {
    }
}
