<?php namespace Must\Leads\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMustLeadsRegisters extends Migration
{
    public function up()
    {
        Schema::create('must_leads_registers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 255);
            $table->string('company', 255);
            $table->string('email', 255);
            $table->string('phone', 255);
            $table->text('message');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('must_leads_registers');
    }
}
