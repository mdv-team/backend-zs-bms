<?php return [
    'plugin' => [
        'name' => 'Leads',
        'description' => ''
    ],
    'validations' => [
        'name_required' => 'O nome é obrigatório',
        'name_string' => 'O nome deve ser um texto',
        'company_required' => 'O nome da empresa é obrigatório',
        'company_string' => 'O nome da empresa deve ser um texto',
        'email_required' => 'Informe um e-mail',
        'email_email' => 'Informe um e-mail válido',
        'phone_required' => 'Informe um número de telefone',
        'phone_regex' => 'Informe um número de telefone válido',
        'message_required' => 'Digite sua mensagem'
    ]
];