<?php return [
    'plugin' => [
        'name' => 'Leads',
        'description' => ''
    ],
    'validations' => [
        'name_required' => 'Name is required',
        'name_string' => 'Name format is not valid',
        'company_required' => 'Company is required',
        'company_string' => 'Company is not valid',
        'email_required' => 'Email address is required',
        'email_email' => 'Email address is not valid',
        'phone_required' => 'Phone number is required',
        'phone_regex' => 'Phone number is not valid',
        'message_required' => 'Message is required'
    ]
];