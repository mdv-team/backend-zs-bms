<?php namespace Must\Leads\Models;

use Model;

/**
 * Model
 */
class Lead extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'must_leads_registers';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
